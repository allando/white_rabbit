<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // Load file
        file_get_contents($filePath);
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // Find total number of occurrences of each letter
        $lowerFile = strtolower($parsedFile); // All letters in file in now lowercase
        $alphabet = range('a', 'z');

        // Counts occurrence of each letter in file
        foreach ($alphabet as $val)  {
            $letter = $val;
            $occurrences = array(substr_count($lowerFile, $val));

            foreach ($occurrences as $item)
                echo $letter . ": " . $item . "<br >";
        }

        // find median

    }

}